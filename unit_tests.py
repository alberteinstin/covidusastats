
# Author: Albert Golembiowski
 
from io import StringIO
from unittest.mock import patch
from classes import *
from funcs import *

import unittest
from contextlib import contextmanager

@contextmanager
def mockInput(my_entry):
    original_input = __builtins__.input
    __builtins__.input = lambda _: my_entry
    yield
    __builtins__.input = original_input

 
all_counties = Counties('us-counties.csv','Geocodes_USA_with_Counties.csv')

class TestFileMethods(unittest.TestCase):
 
    def test_county_input_0(self):
        with mockInput(" Los angeles"):
            self.assertEqual(all_counties.get_valid_county_entry(), ('los angeles','CA'))

    def test_county_input_1(self):
        with mockInput("Clay, iL"):
            self.assertEqual(all_counties.get_valid_county_entry(),('clay','IL'))

    def test_county_input_2(self):
        with mockInput(" Los    angeles, cA "):
            self.assertEqual(all_counties.get_valid_county_entry(), ('los angeles','CA'))

    def test_county_input_3(self):
        with mockInput(" Los angeles"):             # unique counties are mapped np
            self.assertEqual(all_counties.get_valid_county_entry(), ('los angeles','CA'))

    def test_county_input_4(self):
        with mockInput(" cases  "):             # unique counties are mapped np
            self.assertEqual(Counties.get_valid_statistics_type(), ['cases'])

    def test_county_input_5(self):
        with mockInput(" cases , deaths, cases "):             # unique counties are mapped np
            self.assertEqual(Counties.get_valid_statistics_type(), ['cases','deaths'])


    def test_reload_1(self):      # check for exception when dir/file non-existent 
        with self.assertRaises(FileNotFoundError):
            all_counties.init('us_counties.csv')

    def test_reload_2(self):      # check for exception when dir/file non-existent 
        with self.assertRaises(FileNotFoundError):
            all_counties.init('','geocodes-USA_with_counties.csv')


    def test_one_county(self):
        my_radius = Radius(40.0)
        target_county = ('los angeles','CA')
        #  <- (doInteractive,i_stat,counties_within)
        res = all_counties.process_one_county(target_county, my_radius, ['cases'])
        counties_within = from_file_dump('cases_la_40_counties_with.bin')
        # print("test_one_county> counties_within ", counties_within)
        # print("test_one_county> _________within ", res[2])
        self.assertEqual(res[0], False)
        self.assertEqual(res[1][0], 410727)
        self.assertEqual(res[2],counties_within)

if __name__ == '__main__':
    unittest.main()