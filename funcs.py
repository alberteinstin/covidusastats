# Author: Albert Golembiowski
import pickle
import re
import string
# from urllib.request import urlopen 
# from math import sqrt

do_debug = False
# technically 'USstateAbrvs.csv' can be pulled just from the 2 dbs provided 
# # 'us-counties.csv'(County,StateAbrv) and 'Geocodes_USA_with_Counties.csv'(County,StateName) by
#  figuring out the algorithm that produces 'StateName' -> 'StateAbrv' (has to be unique)
# matching (County)
def generate_state_code_lookup():
    state_abrv_codes = open_and_convert2D('USstateAbrvs.csv')
    state_names = [x[0].lower() for x in state_abrv_codes]
    state_abrvs = [x[2] for x in state_abrv_codes]
    state_name_dict = dict(zip(state_names,state_abrvs))        # e.g. {"alabama":"AL", .... }
    state_abrv_dict = dict(zip(state_abrvs,state_names))        # e.g. {"AL":"alabama", .... }

    return state_name_dict, state_abrv_dict
 
def strip_from_end(My_String, Sub_Str):
    str = My_String
    if len(Sub_Str) == 0:
        return str
    if str.endswith(Sub_Str):
        str = str[:-len(Sub_Str)]
    return str

def min_number (n1, n2):
    if n1 <= n2:
        return n1
    return n2

def dump_to_file(File_Path, My_Object):
    with open(File_Path,'wb') as file:
        pickle.dump(My_Object,file)

def from_file_dump(File_Path):
    with open(File_Path,'rb') as file:
        obj = pickle.load(file)
    return obj

# used for normalize county names -> human readable
def display(My_County):
    name = My_County[0]
    state = My_County[1]
    name = string.capwords(name)
    return "'" + name + ", " + state + "'"
    
    # used for list -> human readable
def display_list(My_Entry):
    if len(My_Entry) == 1:
        return My_Entry[0]
    if type(My_Entry[0]) is string:
        fixed = ', '.join(My_Entry)
        return "'" + fixed  + "'"
    else:
        fixed = ', '.join([str(elem) for elem in My_Entry])  #convert to string before joining 
    return fixed

# not used!
def convert2D(strings):
    count = len(strings)
    for i in range(count):
        string_list = strings[i].replace('"' ,'').split(",")  # remove embedded '"' and break up
#        [x.strip('"') for x in string_list]
        strings[i] = string_list   # convert to 2D array
    return strings

# not used!
def open_and_convert2D(path):
    with open(path, "r") as f:
        list_of_strings = [line.rstrip() for line in f][1:]  # remove header
    f.close()
    l2 = convert2D(list_of_strings)     #<-- list of lists
    return l2
 
