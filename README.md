# CovidUSAStats

how to run:
    execute
    > python PycharmProjects/CovidInUSA/main.py

    and interactive prompts should be self-explanatory
    Valid spec for a county is     (<county_name [[cC]ounty], <state_name>)
    e.g. 'los angeles county, Ca' or  'los angeles county, California' or  'los angeles, cA'
    Invalid county spec is flagged, and prompted for correction. It'd be trivial to add more smarts and
    for example accept 'los angeles' or 'Clay' and accept if unique.
    Else a prompt with a filtered list should appear (not implemented yet!)
    
    Entries are self-validating, i.e. will proceed only when a valid value entered.

    Hence, try for example
    
    > Clay, Il
    > cases
    > 100

     will give you how many COVID-19 cases are for county (Clay,IL) within 100 miles,
    and keep you in the interactive loop asking for a new radius entry, e.g. 2.5 radius for same county

    > 2.5
    
    To choose a different county, chose 'e[scape]'
    
    > e

    If interested in more than one stat in one shot ('baltimore' -> 'Baltimore, MD'), say

    > baltimore
    > cases, deaths, confirmed_deaths
    > 10.5
    
   
   
    To break out of the interactive loop, or never get into it, choose 'h[otspots]'

    > h
    > deaths
    > 15
    
    will list 10 (default) COVID-19 hot spots for 'deaths' within 15 miles of each county in the list as epicenter
    
    To list 100 hot spots for 'cases' of COVID-19 within  miles of each county, say
    
    > h=100
    > deaths, confirmed_deaths, cases
    > 2.5

    100 hot spots will be listed sorted by 1s stat entry, here by 'deaths'
    
    To list all hot spots, say 'h=' or 'h=all' in the above

to run unit tests:
    execute
    > python PycharmProjects/CovidInUSA/unit_tests.py

