from funcs import display
#import functools
import re       #regex
from funcs import generate_state_code_lookup, strip_from_end, min_number, display_list

do_debug = False
class County:
    deg_to_miles = (10000.0/90.0) * 1.6
    s_prompt = "-c-> {m}enter county (e.g 'Baldwin, Alabama' or 'Baldwin, AL') or 'q[uit]' or 'h[otspots[=<nbr>,=all]]' > "

    def __init__(self, name, state, Geo_Coords=set(), Covid_Stats=None):
        self.name_state = (name, state)
        self.geo_coords = Geo_Coords        # set of (latitude,longitude) (deg) (may be multiples for different USPS ZIP)
        self.convid_stats = Covid_Stats
        self.loc_coords_miles = set()     # pre-computed 'geo_coords' converted to location coords (miles)
 
       
class Radius:
    s_prompt = "=r=> for {c}, {m}enter sweep radius (miles) (or 'q[uit]' or 'e[scape]') > "
    def __init__(self, val = 0.0):
        self.val = val

    @property
    def val(self):
        return self.__val

    @val.setter
    def val(self,val):
        if val < 0.0:
            self.__val = 0
        elif val > 1000.0:
            self.__val = 1000.0
        else:
            self.__val = val

    @staticmethod
    def get_valid_entry(County_Spec, Msg=''):
        county_spec = County_Spec
        if type(county_spec) is tuple:
            county_spec = display(county_spec)
        str = input(Radius.s_prompt.format(c=county_spec, m=Msg))
        try:
            val = float(str)      #may throw
            if val < 0.0:
               return 0.0   
            elif val > 1000.0:
                return 1000.0
            else:
                return val

        except ValueError:
            if str == 'q' or str == 'quit':
                raise Exception('exiting')
            if str == 'e' or str == 'escape':
                return -2.0
            new_msg = "*** entered invalid '" + str + "', re-"
            return Radius.get_valid_entry(county_spec, new_msg)

        
        
class Counties:
    state_name2abrv = dict()
    state_abrv2name = dict()
    name_at = ['cases', 'deaths', 'confirmed_cases', 'confirmed_deaths', 'probable_cases', 'probable_deaths']
    abrv_at = ['c', 'd', 'cc', 'cd', 'pc', 'pd']          #abrations for above
    look_at = {'cases': 0, 'deaths': 1 ,'confirmed_cases': 2, 'confirmed_deaths': 3, 'probable_cases': 4, 'probable_deaths': 5}

    s_prompt = "-c-> {m}enter county (e.g 'Baldwin, Alabama' or 'Baldwin, AL') or 'q[uit]' or 'h[otspots[=<nbr>,=all]]' > "
    s_print_prompt = "!!! within sweep radius = {r} (miles) of {c} county, total COVID-19 {s}: "
    s_stat_prompt = "-s-> {v}enter stat type ({s}) or 'q[uit]'> " 

    def __init__(self, Convid_stats_path, Geo_coord_path, State_abrv_path=''):
        Counties.__make_state_name_abrv()
        self.convid_stats_path = ""
        self.geo_coord_path = ""
        self.state_abrv_path  = ""
        self.ignored_states = set()  # to be generated
        self.county_dict = dict()   # build county dict (normalized)
        self.counties = set()       # for fast county entry validation
        self.init(Convid_stats_path, Geo_coord_path, State_abrv_path='')
        
    def init(self, Convid_stats_path = '', Geo_coord_path = '', State_abrv_path='', do_Debug = False):
        prg = "init> "
        if Convid_stats_path != '':
             # build county dict (normalized) ( may throw!)
            self.county_dict = open_and_convertCovidStats(Convid_stats_path, Counties.state_name2abrv)
            self.convid_stats_path = Convid_stats_path
 
        if Geo_coord_path != '':
            if len(self.county_dict) == 0:
                raise Exception(" {} `self.county_dict' should not be null; attempting to fill in geo coords!".format(prg))
            # fill it with geo location data (may throw!)
            self.county_dict = open_and_fill_geo_locations(Geo_coord_path,self.county_dict, do_Debug)
            self.geo_coord_path = Geo_coord_path
        # not used yet!
        if State_abrv_path != '':
           self.state_abrv_path  = State_abrv_path

        self.ignored_states = set()                     # to be generated
        self.counties = set(self.county_dict.keys())    # for fast county entry validation


    @staticmethod
    def __make_state_name_abrv():           #lazy eval
        if len(Counties.state_name2abrv) == 0 or len(Counties.state_abrv2name) == 0:
            Counties.state_name2abrv, Counties.state_abrv2name = generate_state_code_lookup()

    #  call recursively till you get a valid statistics spec ()
    @staticmethod   
    def get_valid_statistics_type(msg=""):
        str = input(Counties.s_stat_prompt.format(v=msg,s=display_list(Counties.name_at)))
        str = str.strip(' ')
        if str == 'q' or str == 'quit':             # a candidate for rexep!
            raise Exception('exiting')

        valid_set = set()
        valid_list = []
        str_list = [x.strip() for x in str.split(',')]
        is_valid = True
        for name in str_list:
            if name in Counties.name_at:
                if name in valid_set:       # to eliminate multiples of same entry
                    continue
                valid_set.add(name)
                valid_list.append(name)
            else:
                is_valid = False
                break

        if is_valid:
            return valid_list   # no multiples of same entry

        new_msg = "*** entered invalid '" + str + "', re-"
        return Counties.get_valid_statistics_type(new_msg)


    #  call recursively till you get a valid county spec ()
    def get_valid_county_entry(self, Msg=''):
        loc = ()
        new_msg = ''
        try:
            str = input(Counties.s_prompt.format(m=Msg)).lower()        # enter county spec (e.g 'Baldwin, Alabama')
            str = re.sub("\s+"," ",str)                                 # collapse multiple ' '
            # may handle entries like 'Bald County, AL'
            county = [x.strip(' ') for x in str.split(',')]
            new_msg = "*** entered invalid '" + str + "', re-"

            if len(county) == 0:   #invalid entry (e.g. '' or '   ')
                return self.get_valid_county_entry(new_msg)
            # single entry (e.g. 'q' or 'h=20' )
            if len(county) == 1:
                if str == 'q' or str == 'quit':        # a candidate for rexep!
                    raise Exception('exiting')           
  
                if str == 'h' or str == 'hotspots' or str[0] == 'h':
                    if '=' in str:
                        county = [x.strip(' ') for x in str.split('=')]
                        if county[1] == 'all' or county[1] == '':           # e.g. 'h=', or 'h=all'
                            return ('h','10000')     # any nbr greater than # of counties
                        else:
                            try:
                                max_hotspots = int(county[1])     # may throw
                            except ValueError as e:
                                return self.get_valid_county_entry(new_msg)
                        return ('h',county[1])                # e.g. 'h=30', or 'h=all'
                    return ('h','10')                         # default # of hotspots == 10
                check = [(c[0],c[1]) for c in self.counties if c[0] == county[0]]
                if len(check) == 1:         # county name is unique
                    return check[0]         # strip list, just the tuple (pair)

                return self.get_valid_county_entry(new_msg) # otherwise invalid
            # have 2 csv entries
            state_name = county[1]               #normalize state name
            if len(state_name) == 1:
                raise KeyError(state_name)                 # cannot have a single letter!
            if len(state_name) != 2:                     #assume it's long name and needs convert
                state_name = Counties.state_name2abrv[state_name.lower()]   #normalize state name (may throw)

            state_name = state_name.upper()                   # normalize ( 'Bald County, aL' -> 'Bald County, AL')
            county_name = county[0].lower()
            county_name = strip_from_end(county_name,'county').strip(' ')
            loc = (county_name, state_name)
         
            if loc in self.counties:
                return loc
            # not found in 'self.counties', must be invalid
            return self.get_valid_county_entry(new_msg)

        except KeyError:
            return self.get_valid_county_entry(new_msg)


    def get_counties_within_radius(self, Target_County, My_Radius = 0.0):
        prg = "get_counties_within_radius> "
        counties_within = set()
        counties_within.add(Target_County)      # make sure 'Target_County' is counted!
        
        if My_Radius <= 0:              # handle obvious case efficiently
            return counties_within

        radius_squared = My_Radius**2
        try:
            target_loc = self.county_dict[Target_County]       # look up attributes of 'Target_County'
            # try all counties ----------(county loop)
            for test_county in self.counties:
                test_loc = self.county_dict[test_county]        # look up attributes of 'test_county'
                if len(test_loc.geo_coords) == 0:               # test_county has no geo locations
                    continue                                    # should never happen!
                # iterate over set of geo locations (may be many for different USPS ZIPs)
                for target_geo in target_loc.loc_coords_miles:
                    is_already_included = False
                    for test_geo in test_loc.loc_coords_miles:
                        lati_diff_squared = (target_geo[0] - test_geo[0])**2      #lattitude differnce between target and test
                        long_diff_squared = (target_geo[1] - test_geo[1])**2      #longitude diff (miles)
                        if lati_diff_squared + long_diff_squared <= radius_squared:
                            counties_within.add(test_county)
                            is_already_included = True                      # for efficiency 
                            break
                        if is_already_included:
                            break                                           # break out to (county loop)
            return counties_within
        except KeyError as e:
            raise Exception("*** {p} no entry {e} in county dictionary ! ".format(p=prg, e=Target_County))
 
        return counties_within    #empty

    # <- a list of 'stat's, `Stats_Name` is a list
    def compute_stats(self, Stats_Name, Counties_Within):
        stat = list(0  for i in range(len(Stats_Name)))         #intialize to 0
        stat_names = Stats_Name          # can be indexed

        for county in Counties_Within:
            try:
                this_county = self.county_dict[county]    # may throw if 'c' not in 
                for i in range(len(Stats_Name)):
                    name = stat_names[i]
                    pos = Counties.look_at[name]          
                    stats = this_county.convid_stats
                    if len(stats) > pos and stats[pos] != None:
                        stat[i] += int(stats[pos])    # pick up stats at `pos` (may throw if not valid 'int')
            except KeyError:       # no stats available for 'county;
                continue
            except ValueError:       # no valid 'int' stat at 'pos'
                continue
        return stat

    # -> Stat_Type: a list of names, e.g. ['cases','deaths']
    def process_one_county (self,Target_County, My_Radius, Stat_Type, doInteractive=False):
        len_stats = len(Stat_Type)
        i_stat = list(0  for i in range(len_stats))     # generate of same size as `Target_County`
 #       i_stat = tuple([] for _ in range(len(Target_County)))   # create tuple of same size as `Target_County`
 
        do_while_block = True
        radius = My_Radius.val          #
        target_county = Target_County
        counties_within = []
        if len_stats == 0:                                       # precaution 
            return (doInteractive,i_stat,counties_within)          # should never happen
        # ----------- interactive loop for stats collection ------------------------
        while radius != -2.0 and do_while_block:

            if doInteractive:
                i_stat = list(0  for i in range(len_stats)) # need to reset on each iteration
         
            try:
                if doInteractive:
                    radius = Radius.get_valid_entry(target_county)   # miles from target county (recurse till get right value)
                    if radius == -2.0:                         # 'e[scape]' was entered
                        target_county = self.get_valid_county_entry()  #convert to normalized rep
                        if target_county[0] == 'h':            # asking for 'hotspots', hence break out
                            i_stat[0] = -int(target_county[1])   # overload: indicate how many 'h[otspots]'
                            doInteractive = False
                            counties_within.clear()
                            break
                        radius = Radius.get_valid_entry(target_county)  # re-enter radius
                        if radius == -2.0:                      # 2nd 'e[scape]' was entered
                            doInteractive = False
                            counties_within.clear()
                            continue
                    if target_county[0] == 'h':            # asking for 'hotspots', hence break out
                        i_stat[0] = -int(target_county[1])   # overload: indicate how many 'h[otspots]'
                        doInteractive = False
                        counties_within.clear()
                        break

                if do_debug:
                    print("target county: ",target_county)

                counties_within = self.get_counties_within_radius(target_county, radius)
                if do_debug:
                    print("counties within {r} miles: {w}".format(r=radius, w=counties_within))

                if do_debug:
                    for c in counties_within:
                        try:
                            print(c, ", stats: ", self.county_dict[c])
                        except KeyError as e:
                            print("no stats available for: ", e.args[0])

                #  cummulative stats collected for 'Stat_Type' from 'counties_with' given 'radius'          
                stats = self.compute_stats(Stat_Type, counties_within)          # may throw
                assert len_stats == len(stats)
                for i in range(len_stats):
                    i_stat[i] += stats[i]                   # accumulate

                if doInteractive:
                    print(Counties.s_print_prompt.format(r=radius,c=display(target_county),s=display_list(Stat_Type)), display_list(i_stat)," !!!")
            except ValueError as e:
                continue                # should not happen!
            do_while_block = doInteractive      # in !doInteractive 'while' block is done once!

        return (doInteractive,i_stat,counties_within)

 
# build a dict of (county,state) normalized 'county': lower case, 'state': 2-letter USPS code, upper case
def open_and_convertCovidStats(path, name_to_abrv):
    prg = 'open_and_convertCovidStats > '
    covind_dict = {}
    try:
        f = open(path, "r")                    # may throw
        line_str = f.readline()               # remove header
        fields = []
        i_iter = 0
        stats = []
        while line_str:
            try:
                line_str = f.readline()     # read one line at a time
                if line_str == '':
                    break
                fields = line_str.split(',')
                elem = [field.rstrip() for field in fields]
                county_spec = (elem[1].lower(), name_to_abrv[elem[2].lower()])
                if county_spec[0] == 'unknown':
                    i_iter = i_iter + 1
                    continue
        
                stats = []
                for i in range(4,10):       #convert fields to int
                    if elem[i] == '':
                        stats.append(None)
                    else:
                        stats.append(int(elem[i]))

                # don't have geo location yet, hence 'set()'
                county = County(county_spec[0], county_spec[1], set(), (stats[0], stats[1], stats[2], stats[3], stats[4], stats[5]))
                covind_dict[county.name_state] = county
            except IndexError as e:
                print (prg, "violating access {l} 'line_str' {a}: {f}".format(l=i_iter,a=e.args[0], f=stats))  #ignore
            except KeyError as e:
                print (prg, "unknown key in 'line_str'", e.args[0], fields)
            i_iter = i_iter + 1
        f.close()
    except FileNotFoundError as e:
        print (prg, "{er} `{f}': ".format(er=e.strerror, f=e.filename))
        raise e

    return covind_dict


def open_and_fill_geo_locations(path, County_Dict, do_Debug):
    prg = 'open_and_fill_geo_locations > '
    try:
        f = open(path, "r")                 # may throw
        line_str = f.readline()           # remove header
        fields = []
        while line_str:
            try:
                line_str = f.readline()              # read one line at a time
                if line_str == '':
                    break
                fields = line_str.split(',')
                elem = [field.rstrip() for field in fields]
                county_name = elem[5].lower()
                if county_name == '':                 # county is missing, try to repair
                    county_name = elem[1].lower()    # chances are it's named same as primary city 
                if county_name == 'unknown':         #ignore
                    continue

                state_code = elem[2]
                county_spec = (county_name, state_code)
                geo_coord = (float(elem[3]), float(elem[4]))    # assume db does not contain bad coords, e.g. ',,'
                if geo_coord[0] == 0.0 or geo_coord[1] == 0.0:  # ignore entries with non-valid geo coord
                    continue                                    # e.g. '96677,Fpo,AP,0,0,,MILITARY,NA,US,0,0,'
                county = County_Dict[county_spec]               # may throw
                county.geo_coords.add(geo_coord)               # collapses multiple same entries (for different zip code) into one
                loc_coord_miles = (geo_coord[0] * County.deg_to_miles, geo_coord[1] * County.deg_to_miles) # pre-compute for efficiency 
                county.loc_coords_miles.add(loc_coord_miles)
 #               list_coord_miles = list(county.loc_coords_miles)
            
            except IndexError as e:
                if line_str != '\n':      #ignore empty lines
                    print (prg,"violating accsed 'line_str'", e.args[0], fields)   #ignore
            except KeyError as e:
                if do_Debug:
                    print (prg,"unknown county lookup key: ", e.args[0])
        f.close()

    except FileNotFoundError as e:
        print (prg, "{er} `{f}': ".format(er=e.strerror, f=e.filename))
        raise e

    return County_Dict

   # 'County_List' is a list of tuples (county_spec,stats), where `stats' is a list 
def process_hot_spots(County_List, Stat_Type, My_Radius, Max_Nbr, do_Debug = False):
    hot_spots = County_List
    max_top_hot_nbr = Max_Nbr
    # list of tuples (county_spec,stats), where `stats' is a tuple 
    hot_spots.sort(key=lambda j: -j[1][0])   # descending order, sort on `stats[0]`

    if do_Debug:
        print("sorted \n", hot_spots)

    iter = 0
    max_top_hot_nbr = min_number(max_top_hot_nbr, len(hot_spots))    #only this many hotspots to output
    print("\n------------- for '{s}', sorting for top {m} hotspots (radius = {r} miles) on '{p}' -------------\n".format(m=max_top_hot_nbr,s=display_list(Stat_Type),r=My_Radius.val, p=Stat_Type[0]))
    for hot in hot_spots:
        stats = hot[1]
        if iter >= max_top_hot_nbr or all([ v == 0 for v in stats]):       # bail if max or no more cases (stats all 0) 
            break
        print("in county {c} this many {s}: {v} ".format(c=display(hot[0]), v=display_list(stats), s=display_list(Stat_Type)))
        iter = iter + 1
     
    print("\n------------- ({s}) top {m} hotspots reported (radius = {r} miles) -------------\n".format(m=iter,s=display_list(Stat_Type),r=My_Radius.val))
 