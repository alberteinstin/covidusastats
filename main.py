# Author: Albert Golembiowski
 
from classes import *
from funcs import *

do_debug = False
# Press the green button in the gutter to run the script.
if __name__ == '__main__':

    prg = "main> "
    try:
        all_counties = Counties('us-counties.csv','Geocodes_USA_with_Counties.csv')

        if all_counties.ignored_states:
            print("*** ignoring unofficial states: ", all_counties.ignored_states, " ***")
    
    # ------------------ input ---------------------------------

        do_interactive = True
        max_top_hot_nbr = 10    # only this many hotspots to output (default)
        # a test (non-interactive)
 #       val = all_counties.process_one_county(('los angeles','CA'), Radius(40.0), ['cases'])

        # enter county spec (e.g 'Baldwin, Alabama')
        target_county = all_counties.get_valid_county_entry()   #convert to normalized rep
        if target_county[0] == 'h':                        # choose to process hotsposts
            do_interactive = False                         # switch to non-interactive for hotspots
            max_top_hot_nbr = int(target_county[1])        # nbr of hot spots to display
            display_county = "each county hotspot tested"
        else:
            display_county = display(target_county)
        # <- a list of names, e.g. ['cases','deaths']
        stat_type = Counties.get_valid_statistics_type("for " + display_county + ", ")           #recurse till valid or 'q'
 
        my_radius = Radius(0.0)
        result = (do_interactive,[0])
        
    #   :::::::::::::::::: process ::::::::::::::::::::::::::::::
        if do_interactive:
            # <- result[0] == False, if breaking out of interactivity
            result = all_counties.process_one_county(target_county, my_radius, stat_type, do_interactive)
 #           dump_to_file('cases_la_40_counties_with.bin',result[2])

        # -------------------- hotspots processing --------------------------------h
        if not result[0]:      # same as 'not do_interactive'
            if result[1][0] < 0:
                max_top_hot_nbr = -result[1][0]                         # overload: indicate how many 'h[otspots]'
            radius_val = Radius.get_valid_entry("each county hotspot tested")
            if radius_val == -2.0:                         # code for `e[scape]`, same as `q[uit]`
                raise Exception('exiting')
            radius = Radius(radius_val)   # miles from each county (recurse till get right value)
            iter = 0
            county_stat_list = []
            print ("\n------------ scanning {n} counties within radius = {r} (miles) for CONVID-19 '{t}' -------".format(n=len(all_counties.counties),r=radius.val,t=display_list(stat_type)))
            for county in all_counties.counties:
                # collect stats of 'stat_type' for 'county' as epicenter
                ret = all_counties.process_one_county(county, radius, stat_type)
                i_stat = ret[1]                                      # pick up stats for current 'county'
                county_stat_list.append((county,i_stat))          # tuples (county_spec,stats), where `stats' is a list 
                if do_debug:
                    print("main> ", Counties.s_print_prompt.format(i=iter, r=radius.val,c=county,s=stat_type, nc=len(ret[2]), n=ret[2]), i_stat," <--")
                iter = iter + 1
            if do_debug:
                print("hot spots:", county_stat_list)
            # sort and display 
            process_hot_spots(county_stat_list, stat_type, radius, max_top_hot_nbr, do_debug)
             
    except BaseException as e:
        msg = str(e)
        if msg == 'exiting':
            exit(0)
        print(prg, "caught exception: ", msg, " (exiting) ")
        exit(-1)
    
